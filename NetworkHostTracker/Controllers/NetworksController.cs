﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;

namespace NetworkHostTracker.Controllers
{
    public class NetworksController : Controller
    {
        private NetHostTrackerContext db = new NetHostTrackerContext();

        // GET: Networks
        public async Task<ActionResult> Index()
        {
            return View(await db.Networks.ToListAsync());
        }

        // GET: Networks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Network network = await db.Networks.FindAsync(id);
            if (network == null)
            {
                return HttpNotFound();
            }
            return View(network);
        }

        // GET: Networks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Networks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,IpAddress,NetMask,DefaultGateway")] Network network)
        {
            if (ModelState.IsValid)
            {
                var testNetwork = db.Locations.FirstOrDefault(e => e.Name == network.Name && e.Id != network.Id);
                if (testNetwork == null)
                {
                    db.Networks.Add(network);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A network with the same name already exists.");
            }
            return View(network);
        }

        // GET: Networks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Network network = await db.Networks.FindAsync(id);
            if (network == null)
            {
                return HttpNotFound();
            }
            return View(network);
        }

        // POST: Networks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,IpAddress,NetMask,DefaultGateway")] Network network)
        {
            if (ModelState.IsValid)
            {
                var testNetwork = db.Locations.FirstOrDefault(e => e.Name == network.Name);
                if (testNetwork == null)
                {
                    db.Entry(network).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { id = network.Id });
                }
                ModelState.AddModelError("", "A network with the same name already exists.");
                return View(network);
            }
            return View(network);
        }

        // GET: Networks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Network network = await db.Networks.FindAsync(id);
            if (network == null)
            {
                return HttpNotFound();
            }
            return View(network);
        }

        // POST: Networks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Network network = await db.Networks.FindAsync(id);
            var networkHostAssignments = db.HostAssignments.Where(e => e.NetworkId == network.Id).ToList();
            if (networkHostAssignments.Count() > 0)
            {
                ModelState.AddModelError("", $"This operation cannot be completed because the network has {networkHostAssignments.Count()} host/s associated with it.");
                return View(network);
            }
            var hostsWithAsDefault = db.Hosts.Where(e => e.DefaultNetwork == network.Id).ToList();
            if (hostsWithAsDefault.Count() > 0)
            {
                ModelState.AddModelError("", $"This operation cannot be completed because the network is set as the default network for {hostsWithAsDefault.Count()} host/s.");
                return View(network);
            }
            db.Networks.Remove(network);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
