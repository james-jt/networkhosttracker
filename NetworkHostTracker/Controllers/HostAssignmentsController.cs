﻿using Model;
using NetworkHostTracker.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NetworkHostTracker.Controllers
{
    public class HostAssignmentsController : Controller
    {
        private NetHostTrackerContext db = new NetHostTrackerContext();

        // GET: HostAssignments
        public async Task<ActionResult> Index()
        {
            var hostAssignments = db.HostAssignments.Include(h => h.Host).Include(h => h.Network);
            return View(await hostAssignments.ToListAsync());
        }

        // GET: HostAssignments/Details/5
        public async Task<ActionResult> Details(int? hostId, int? networkId, string ipAddress)
        {
            if (hostId == null || networkId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HostAssignment hostAssignment = await db.HostAssignments.FindAsync(networkId, hostId, ipAddress);
            if (hostAssignment == null)
            {
                return HttpNotFound();
            }
            return View(hostAssignment);
        }

        // GET: HostAssignments/Create
        public ActionResult Create()
        {
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name");
            ViewBag.NetworkId = new SelectList(db.Networks, "Id", "Name");
            return View();
        }

        // POST: HostAssignments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "NetworkId,HostId,IpAddress")] HostAssignment hostAssignment)
        {
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name", hostAssignment.HostId);
            ViewBag.NetworkId = new SelectList(db.Networks, "Id", "Name", hostAssignment.NetworkId);
            if (ModelState.IsValid)
            {
                if (!BusinessLogic.BusinessLogic.IsIpAddressValid(hostAssignment.IpAddress))
                {
                    ModelState.AddModelError("", "The IP address you entered is invalid.");
                    return View(hostAssignment);
                }
                //Check if this assignment does not already exist i.e. the combination of host, network and IP address
                var host = await db.Hosts.FindAsync(hostAssignment.HostId);
                var network = await db.Networks.FindAsync(hostAssignment.NetworkId);
                var oldHostAssignment = await db.HostAssignments.FindAsync(hostAssignment.NetworkId, hostAssignment.HostId, hostAssignment.IpAddress);
                if (oldHostAssignment == null)
                {
                    //Check if IP address belongs to this subnet
                    if (BusinessLogic.BusinessLogic.IsIpAddressInNetwork(network, hostAssignment.IpAddress))
                    {
                        //Check if IP address is free
                        var testAssignment = db.HostAssignments.FirstOrDefault(e => e.NetworkId == hostAssignment.NetworkId && e.IpAddress == hostAssignment.IpAddress);
                        if (testAssignment == null)
                        {
                            hostAssignment.DateAssigned = DateTime.Now.GetCondensedDateTime();
                            db.HostAssignments.Add(hostAssignment);
                            await db.SaveChangesAsync();
                            return RedirectToAction("Index");
                        }
                        ModelState.AddModelError("", "The IP address you entered has already been assigned to another host.");
                        return View(hostAssignment);
                    }
                    ModelState.AddModelError("", "The IP address you entered does not belong to the network you selected.");
                    return View(hostAssignment);
                }
                ModelState.AddModelError("", "This host already belongs to this network with the specified IP address.");
                return View(hostAssignment);
            }
            return View(hostAssignment);
        }

        // GET: HostAssignments/Edit/5
        public async Task<ActionResult> Edit(int? hostId, int? networkId, string ipAddress)
        {
            if (hostId == null || networkId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HostAssignment hostAssignment = await db.HostAssignments.FindAsync(networkId, hostId, ipAddress);
            if (hostAssignment == null)
            {
                return HttpNotFound();
            }
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name", hostAssignment.HostId);
            ViewBag.NetworkId = new SelectList(db.Networks, "Id", "Name", hostAssignment.NetworkId);
            return View(hostAssignment);
        }

        // POST: HostAssignments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "NetworkId,HostId,IpAddress,DateAssigned")] HostAssignment hostAssignment)
        {
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name", hostAssignment.HostId);
            ViewBag.NetworkId = new SelectList(db.Networks, "Id", "Name", hostAssignment.NetworkId);
            if (ModelState.IsValid)
            {
                if (!BusinessLogic.BusinessLogic.IsIpAddressValid(hostAssignment.IpAddress))
                {
                    ModelState.AddModelError("", "The IP address you entered is invalid.");
                    return View(hostAssignment);
                }
                var host = await db.Hosts.FindAsync(hostAssignment.HostId);
                var network = await db.Networks.FindAsync(hostAssignment.NetworkId);
                var oldHostAssignment = await db.HostAssignments.FindAsync(hostAssignment.NetworkId, hostAssignment.HostId, hostAssignment.IpAddress);
                if (oldHostAssignment == null)
                {
                    //Check if IP address belongs to this subnet
                    if (BusinessLogic.BusinessLogic.IsIpAddressInNetwork(network, hostAssignment.IpAddress))
                    {
                        //Check if IP address is free
                        var testAssignment = db.HostAssignments.FirstOrDefault(e => e.NetworkId == hostAssignment.NetworkId && e.IpAddress == hostAssignment.IpAddress);
                        if (testAssignment == null)
                        {
                            var tempAssignment = db.HostAssignments.FirstOrDefault(e => e.NetworkId == hostAssignment.NetworkId && e.HostId == hostAssignment.HostId && e.DateAssigned == hostAssignment.DateAssigned);
                            //tempAssignment = await db.HostAssignments.FindAsync(hostAssignment.NetworkId, hostAssignment.HostId, tempAssignment.IpAddress);
                            //tempAssignment.IpAddress = hostAssignment.IpAddress;
                            //tempAssignment.DateAssigned = DateTime.Now.GetCondensedDateTime();
                            hostAssignment.DateAssigned = DateTime.Now.GetCondensedDateTime();
                            db.Entry(tempAssignment).State = EntityState.Deleted;
                            db.Entry(hostAssignment).State = EntityState.Added;
                            if (host.DefaultNetwork == hostAssignment.NetworkId)
                            {
                                host.DefaultNetworkIpAddress = hostAssignment.IpAddress;
                                db.Entry(host).State = EntityState.Modified;
                            }
                            await db.SaveChangesAsync();
                            return RedirectToAction("Index");
                        }
                        ModelState.AddModelError("", "The IP address you entered has already been assigned to another host.");
                        return View(hostAssignment);
                    }
                    ModelState.AddModelError("", "The IP address you entered does not belong to the network you selected.");
                    return View(hostAssignment);
                }
                ModelState.AddModelError("", "This host already belongs to this network with the specified IP address.");
                return View(hostAssignment);
        }
            return View(hostAssignment);
        }

        // GET: HostAssignments/Delete/5
        public async Task<ActionResult> Delete(int? hostId, int? networkId, string ipAddress)
        {
            if (hostId == null || networkId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HostAssignment hostAssignment = await db.HostAssignments.FindAsync(networkId, hostId, ipAddress);
            if (hostAssignment == null)
            {
                return HttpNotFound();
            }
            return View(hostAssignment);
        }

        // POST: HostAssignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int hostId, int networkId, string ipAddress)
        {
            HostAssignment hostAssignment = await db.HostAssignments.FindAsync(networkId, hostId, ipAddress);
            var host = await db.Hosts.FindAsync(hostId);
            if(host.DefaultNetwork == networkId)
            {
                ModelState.AddModelError("", "This association defines the host's default network. Please assign a new default network for the host first.");
                return View(hostAssignment);
            }
            db.HostAssignments.Remove(hostAssignment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
