﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;

namespace NetworkHostTracker.Controllers
{
    public class HostTypesController : Controller
    {
        private NetHostTrackerContext db = new NetHostTrackerContext();

        // GET: HostTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.HostTypes.ToListAsync());
        }

        // GET: HostTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HostType hostType = await db.HostTypes.FindAsync(id);
            if (hostType == null)
            {
                return HttpNotFound();
            }
            return View(hostType);
        }

        // GET: HostTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HostTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description")] HostType hostType)
        {
            if (ModelState.IsValid)
            {
                var testHostType = db.Locations.FirstOrDefault(e => e.Name == hostType.Name);
                if (testHostType == null)
                {
                    db.HostTypes.Add(hostType);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A host type with the same name already exists.");
            }

            return View(hostType);
        }

        // GET: HostTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HostType hostType = await db.HostTypes.FindAsync(id);
            if (hostType == null)
            {
                return HttpNotFound();
            }
            return View(hostType);
        }

        // POST: HostTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] HostType hostType)
        {
            if (ModelState.IsValid)
            {
                var testHostType = db.Locations.FirstOrDefault(e => e.Name == hostType.Name && e.Id != hostType.Id);
                if (testHostType == null)
                {
                    db.Entry(hostType).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { id = hostType.Id });
                }
                ModelState.AddModelError("", "A host type with the same name already exists.");
            }
            return View(hostType);
        }

        // GET: HostTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HostType hostType = await db.HostTypes.FindAsync(id);
            if (hostType == null)
            {
                return HttpNotFound();
            }
            return View(hostType);
        }

        // POST: HostTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HostType hostType = await db.HostTypes.FindAsync(id);
            if (hostType.Hosts.Count() > 0)
            {
                ModelState.AddModelError("", "This operation cannot be completed because the host type has hosts associated with it.");
                return View(hostType);
            }
            db.HostTypes.Remove(hostType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
