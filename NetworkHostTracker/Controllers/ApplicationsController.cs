﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;
using NetworkHostTracker.Models;

namespace NetworkHostTracker.Controllers
{
    public class ApplicationsController : Controller
    {
        private NetHostTrackerContext db = new NetHostTrackerContext();

        // GET: Applications
        public async Task<ActionResult> Index()
        {
            return View(await db.Applications.ToListAsync());
        }

        // GET: Applications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = await db.Applications.FindAsync(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // GET: Applications/Create
        public ActionResult Create()
        {
            ViewBag.PrimaryHost = new SelectList(db.Hosts, "Id", "Name");
            return View();
        }

        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,PrimaryHost")] Application application)
        {
            ViewBag.PrimaryHost = new SelectList(db.Hosts, "Id", "Name", application.PrimaryHost);
            if (ModelState.IsValid)
            {
                var testApplication = db.Applications.FirstOrDefault(e => e.Name == application.Name);
                if (testApplication == null)
                {
                    db.Applications.Add(application);
                    await db.SaveChangesAsync();
                    application = db.Applications.FirstOrDefault(e => e.Name == application.Name);
                    var applicationAssignment = new ApplicationAssignment();
                    applicationAssignment.ApplicationId = application.Id;
                    applicationAssignment.DateAssigned = DateTime.Now.GetCondensedDateTime();
                    applicationAssignment.HostId = application.PrimaryHost;
                    db.ApplicationAssignments.Add(applicationAssignment);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "An application with the same name already exists.");
                return View(application);

            }

            return View(application);
        }

        // GET: Applications/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = await db.Applications.FindAsync(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            var applicationHosts = new List<int>();
            db.ApplicationAssignments.Where(e => e.ApplicationId == id).ToList().ForEach(e => applicationHosts.Add(e.HostId));
            ViewBag.PrimaryHost = new SelectList(db.Hosts.Where(e => applicationHosts.Contains(e.Id)), "Id", "Name", application.PrimaryHost);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,PrimaryHost")] Application application)
        {
            var applicationHosts = new List<int>();
            db.ApplicationAssignments.Where(e => e.ApplicationId == application.Id).ToList().ForEach(e => applicationHosts.Add(e.HostId));
            ViewBag.PrimaryHost = new SelectList(db.Hosts.Where(e => applicationHosts.Contains(e.Id)), "Id", "Name", application.PrimaryHost);
            if (ModelState.IsValid)
            {
                var testApplication = db.Applications.FirstOrDefault(e => e.Name == application.Name && e.Id != application.Id);
                if (testApplication == null)
                {

                    db.Entry(application).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { id = application.Id });
                }
                ModelState.AddModelError("", "An application with the same name already exists.");
                return View(application);
            }
            return View(application);
        }

        // GET: Applications/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = await db.Applications.FindAsync(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Application application = await db.Applications.FindAsync(id);
            db.Applications.Remove(application);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
