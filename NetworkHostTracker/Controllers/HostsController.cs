﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;
using NetworkHostTracker.Models;

namespace NetworkHostTracker.Controllers
{
    public class HostsController : Controller
    {
        private NetHostTrackerContext db = new NetHostTrackerContext();

        // GET: Hosts
        public async Task<ActionResult> Index()
        {
            return View(await db.Hosts.ToListAsync());
        }

        // GET: Hosts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Host host = await db.Hosts.FindAsync(id);
            if (host == null)
            {
                return HttpNotFound();
            }
            return View(host);
        }

        // GET: Hosts/Create
        public ActionResult Create()
        {
            ViewBag.DefaultNetwork = new SelectList(db.Networks, "Id", "Name");
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "Name");
            ViewBag.HostTypeId = new SelectList(db.HostTypes, "Id", "Name");
            ViewBag.PhysicalHostId = new SelectList(db.Hosts.Where(e => e.IsPhysical == true), "Id", "Name");

            return View();
        }

        // POST: Hosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,DomainName,IpAddress,DefaultNetwork,DefaultNetworkIpAddress,HostTypeId,Make,LocationId,IsPhysical,PhyisicalHostId")] Host host)
        {
            ViewBag.DefaultNetwork = new SelectList(db.Networks, "Id", "Name", host.DefaultNetwork);
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "Name", host.LocationId);
            ViewBag.HostTypeId = new SelectList(db.HostTypes, "Id", "Name", host.HostTypeId);
            ViewBag.PhysicalHostId = new SelectList(db.Hosts.Where(e => e.IsPhysical == true), "Id", "Name", host.PhysicalHostId);

            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(host.DefaultNetworkIpAddress))
                {
                    ModelState.AddModelError("", "Please specify the IP address of the host in the default network.");
                    return View(host);
                }
                var testHost = db.Locations.FirstOrDefault(e => e.Name == host.Name);
                if (testHost == null)
                {
                    var testHost2 = db.Locations.FirstOrDefault(e => e.Name == host.DomainName);
                    if (testHost2 == null)
                    {
                        db.Hosts.Add(host);
                        //Check if IP address belongs to this subnet
                        var network = await db.Networks.FindAsync(host.DefaultNetwork);
                        try
                        {
                            if (BusinessLogic.BusinessLogic.IsIpAddressInNetwork(network, host.DefaultNetworkIpAddress))
                            {
                                //Check if IP address is free
                                var testAssignment = db.HostAssignments.FirstOrDefault(e => e.NetworkId == network.Id && e.IpAddress == host.DefaultNetworkIpAddress);
                                if (testAssignment == null)
                                {
                                    var hostAssignment = new HostAssignment();
                                    hostAssignment.HostId = host.Id;
                                    hostAssignment.NetworkId = host.DefaultNetwork;
                                    hostAssignment.IpAddress = host.DefaultNetworkIpAddress;
                                    hostAssignment.DateAssigned = DateTime.Now.GetCondensedDateTime();
                                    db.HostAssignments.Add(hostAssignment);
                                    await db.SaveChangesAsync();
                                    return RedirectToAction("Index");
                                }
                                ModelState.AddModelError("", "The IP address you entered has already been assigned to another host.");
                                return View(host);
                            }
                            ModelState.AddModelError("", "The IP address you entered does not belong to the network you selected.");
                            return View(host);
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", e.Message);
                            return View(host);
                        }
                    }
                    ModelState.AddModelError("", "A host with the same domain name already exists.");
                    return View(host);
                }
                ModelState.AddModelError("", "A host with the same name already exists.");
                return View(host);
            }
            return View(host);
        }

        // GET: Hosts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Host host = await db.Hosts.FindAsync(id);
            if (host == null)
            {
                return HttpNotFound();
            }
            var hostNetworks = new List<int>();
            db.HostAssignments.Where(e => e.HostId == id).ToList().ForEach(e => hostNetworks.Add(e.NetworkId));
            ViewBag.DefaultNetwork = new SelectList(db.Networks.Where(e => hostNetworks.Contains(e.Id)), "Id", "Name", host.DefaultNetwork);
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "Name", host.LocationId);
            ViewBag.HostTypeId = new SelectList(db.HostTypes, "Id", "Name", host.HostTypeId);
            ViewBag.PhysicalHostId = new SelectList(db.Hosts.Where(e => e.IsPhysical == true), "Id", "Name", host.PhysicalHostId);

            return View(host);
        }

        // POST: Hosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,DomainName,IpAddress,DefaultNetwork,DefaultNetworkIpAddress,HostTypeId,Make,LocationId,IsPhysical,PhysicalHostId")] Host host)
        {
            var hostNetworks = new List<int>();
            db.HostAssignments.Where(e => e.HostId == host.Id).ToList().ForEach(e => hostNetworks.Add(e.NetworkId));
            ViewBag.DefaultNetwork = new SelectList(db.Networks.Where(e => hostNetworks.Contains(e.Id)), "Id", "Name", host.DefaultNetwork);
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "Name", host.LocationId);
            ViewBag.HostTypeId = new SelectList(db.HostTypes, "Id", "Name", host.HostTypeId);
            ViewBag.PhysicalHostId = new SelectList(db.Hosts.Where(e => e.IsPhysical == true), "Id", "Name", host.PhysicalHostId);

            if (ModelState.IsValid)
            {
                var testHost = db.Locations.FirstOrDefault(e => e.Name == host.Name && e.Id != host.Id);
                if (testHost == null)
                {
                    var testHost2 = db.Locations.FirstOrDefault(e => e.Name == host.DomainName && e.Id != host.Id);
                    if (testHost2 == null)
                    {
                        var hostAssignment = db.HostAssignments.Find(host.DefaultNetwork, host.Id, host.DefaultNetworkIpAddress);
                        host.DefaultNetworkIpAddress = hostAssignment.IpAddress;
                        db.Entry(host).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Details", new { id = host.Id });
                    }
                    ModelState.AddModelError("", "A host with the same domain name already exists.");
                }
                ModelState.AddModelError("", "A host with the same name already exists.");
            }
            return View(host);
        }

        // GET: Hosts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Host host = await db.Hosts.FindAsync(id);
            if (host == null)
            {
                return HttpNotFound();
            }
            return View(host);
        }

        // POST: Hosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Host host = await db.Hosts.FindAsync(id);
            var applicationHostAssignments = db.ApplicationAssignments.Where(e => e.HostId == host.Id).ToList();
            if (applicationHostAssignments.Count() > 0)
            {
                ModelState.AddModelError("", $"This operation cannot be completed because the host has {applicationHostAssignments.Count()} application/s associated with it.");
                return View(host);
            }
            var applicationsWithAsDefault = db.Applications.Where(e => e.PrimaryHost == host.Id).ToList();
            if (applicationsWithAsDefault.Count() > 0)
            {
                ModelState.AddModelError("", $"This operation cannot be completed because the host is set as the primary host for {applicationsWithAsDefault.Count()} application/s.");
                return View(host);
            }
            if (host.VirtualMachines.Count() > 0)
            {
                ModelState.AddModelError("", $"This operation cannot be completed because the host is hosting one or more virtual machines.");
                return View(host);
            }

            db.Hosts.Remove(host);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
