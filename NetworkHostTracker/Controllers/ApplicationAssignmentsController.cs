﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;
using NetworkHostTracker.Models;

namespace NetworkHostTracker.Controllers
{
    public class ApplicationAssignmentsController : Controller
    {
        private NetHostTrackerContext db = new NetHostTrackerContext();

        // GET: ApplicationAssignments
        public async Task<ActionResult> Index()
        {
            var applicationAssignments = db.ApplicationAssignments.Include(a => a.Application).Include(a => a.Host);
            return View(await applicationAssignments.ToListAsync());
        }

        // GET: ApplicationAssignments/Details/5
        public async Task<ActionResult> Details(int? applicationId, int? hostId)
        {
            if (applicationId == null || hostId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationAssignment applicationAssignment = await db.ApplicationAssignments.FindAsync(applicationId, hostId);
            if (applicationAssignment == null)
            {
                return HttpNotFound();
            }
            return View(applicationAssignment);
        }

        // GET: ApplicationAssignments/Create
        public ActionResult Create()
        {
            ViewBag.ApplicationId = new SelectList(db.Applications, "Id", "Name");
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name");
            return View();
        }

        // POST: ApplicationAssignments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ApplicationId,HostId,DateAssigned")] ApplicationAssignment applicationAssignment)
        {
            ViewBag.ApplicationId = new SelectList(db.Applications, "Id", "Name");
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name");
            if (ModelState.IsValid)
            {
                var testAssignment = await db.ApplicationAssignments.FindAsync(applicationAssignment.ApplicationId, applicationAssignment.HostId);
                if (testAssignment == null)
                {
                    applicationAssignment.DateAssigned = DateTime.Now.GetCondensedDateTime();
                    db.ApplicationAssignments.Add(applicationAssignment);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "The specified application is already assigned to the selected host.");
                return View(applicationAssignment);
            }
            ViewBag.ApplicationId = new SelectList(db.Applications, "Id", "Name", applicationAssignment.ApplicationId);
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name", applicationAssignment.HostId);
            return View(applicationAssignment);
        }

        // GET: ApplicationAssignments/Edit/5
        public async Task<ActionResult> Edit(int? applicationId, int? hostId)
        {
            if (applicationId == null || hostId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationAssignment applicationAssignment = await db.ApplicationAssignments.FindAsync(applicationId, hostId);
            if (applicationAssignment == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationId = new SelectList(db.Applications, "Id", "Name", applicationAssignment.ApplicationId);
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name", applicationAssignment.HostId);
            return View(applicationAssignment);
        }

        // POST: ApplicationAssignments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ApplicationId,HostId,DateAssigned")] ApplicationAssignment applicationAssignment)
        {
            ViewBag.ApplicationId = new SelectList(db.Applications, "Id", "Name", applicationAssignment.ApplicationId);
            ViewBag.HostId = new SelectList(db.Hosts, "Id", "Name", applicationAssignment.HostId);
            if (ModelState.IsValid)
            {
                var testAssignment = db.ApplicationAssignments.FirstOrDefault(e => e.ApplicationId == applicationAssignment.ApplicationId && e.HostId == applicationAssignment.HostId);
                if (testAssignment == null)
                {
                    db.Entry(applicationAssignment).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "The specified application is already assigned to the selected host.");
                return View(applicationAssignment);
            }
            return View(applicationAssignment);
        }

        // GET: ApplicationAssignments/Delete/5
        public async Task<ActionResult> Delete(int? applicationId, int? hostId)
        {
            if (applicationId == null || hostId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationAssignment applicationAssignment = await db.ApplicationAssignments.FindAsync(applicationId, hostId);
            if (applicationAssignment == null)
            {
                return HttpNotFound();
            }
            return View(applicationAssignment);
        }

        // POST: ApplicationAssignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int applicationId, int hostId)
        {
            ApplicationAssignment applicationAssignment = await db.ApplicationAssignments.FindAsync(applicationId, hostId);
            var application = await db.Applications.FindAsync(applicationId);
            if (application.PrimaryHost == hostId)
            {
                ModelState.AddModelError("", "This association defines the application's primary host. Please assign a new primary host for the application first.");
                return View(applicationAssignment);
            }
            db.ApplicationAssignments.Remove(applicationAssignment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
