﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetworkHostTracker.BusinessLogic
{
    public class BusinessLogic
    {
        public static bool IsIpAddressValid(string ipAddress)
        {
            bool response = true;
            var ipAddressParts = ipAddress.Trim().Split('.');
            if (ipAddressParts.Length != 4)
                response = false;
            foreach(var ipAddressPart in ipAddressParts)
            {
                int integerAddressPart;
                if(Int32.TryParse(ipAddressPart, out integerAddressPart))
                {
                    if (integerAddressPart > 255 || integerAddressPart < 0)
                        response = false;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public static bool IsIpAddressInNetwork(Network network, string ipAddress)
        {
            //Bitwise-AND IpAddress and NetMask and then Network Address and Mask; the two results must be equal.
            //Convert the dotted-decimal addresses to binary and perform ANDs
            var mask = dottedDecimalIpAddressToInt(network.NetMask);
            var netAddress = dottedDecimalIpAddressToInt(network.IpAddress);
            var hostIpAddress = dottedDecimalIpAddressToInt(ipAddress);
            if ((netAddress & mask) == (hostIpAddress & mask))
                return true;
            return false;
        }

        public static uint dottedDecimalIpAddressToInt(string input)
        {
            var binaryIpAddress = String.Join("", (input.Split('.').Select(x => Convert.ToString(Int32.Parse(x), 2).PadLeft(8, '0'))).ToArray());
            uint integerIpAddress = Convert.ToUInt32(binaryIpAddress, 2);
            return integerIpAddress;
        }

    }
}