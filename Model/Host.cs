﻿namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Host
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Host()
        {
            Networks = new HashSet<Network>();
            Applications = new HashSet<Application>();
            VirtualMachines = new HashSet<Host>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Index("DomainNameAndName", 0)]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        [Display(Name = "Domain Name")]
        [Index("DomainNameAndName", 1, IsUnique = true)]
        [StringLength(64)]
        public string DomainName { get; set; }

        //[Required, RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "Please enter a valid IP address.")]
        //[Display(Name = "IP Address")]
        //public string IpAddress { get; set; }

        [Required]
        [Display(Name = "Default Network")]
        public int DefaultNetwork { get; set; }

        [StringLength(16)]
        [Display(Name = "Default Network IP Address")]
        public string DefaultNetworkIpAddress { get; set; }

        [StringLength(64)]
        public string Make { get; set; }

        [Required]
        [Display(Name = "Location")]
        public int LocationId { get; set; }

        [Required]
        [Display(Name = "Host Type")]
        public int HostTypeId { get; set; }

        [Required]
        [Display(Name = "Is Physical")]
        public bool IsPhysical { get; set; }

        [Display(Name = "Physical Host")]
        public int PhysicalHostId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Network> Networks { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<Host> VirtualMachines { get; set; }
        public virtual Location Location { get; set; }
        public virtual HostType HostType { get; set; }
        public virtual Host PhysicalHost { get; set; }

    }
}
 