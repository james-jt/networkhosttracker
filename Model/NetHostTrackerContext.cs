namespace Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public class NetHostTrackerContext : DbContext
    {
        public NetHostTrackerContext()
            : base("name=NetHostTrackerContext")
        {
        }
          
        public virtual DbSet<Host> Hosts { get; set; }
        public virtual DbSet<Network> Networks { get; set; }
        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<HostType> HostTypes { get; set; }
        public virtual DbSet<HostAssignment> HostAssignments { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<ApplicationAssignment> ApplicationAssignments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Host>()
                .HasMany(e => e.Networks)
                .WithMany(e => e.Hosts)
                .Map(m => m.ToTable("HostAssignments").MapLeftKey("HostId").MapRightKey("NetworkId"));

            modelBuilder.Entity<Application>()
                .HasMany(e => e.Hosts)
                .WithMany(e => e.Applications)
                .Map(m => m.ToTable("ApplicationAssignments").MapLeftKey("ApplicationId").MapRightKey("HostId"));

            modelBuilder.Entity<Location>()
                .HasMany(e => e.Hosts)
                .WithRequired(e => e.Location)
                .HasForeignKey(e => e.LocationId);

            modelBuilder.Entity<HostType>()
                .HasMany(e => e.Hosts)
                .WithRequired(e => e.HostType)
                .HasForeignKey(e => e.HostTypeId);

            modelBuilder.Entity<Host>()
                .HasMany(e => e.VirtualMachines)
                .WithRequired(e => e.PhysicalHost)
                .HasForeignKey(e => e.PhysicalHostId);

        }

    }
}
