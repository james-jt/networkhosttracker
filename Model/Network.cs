﻿namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class Network
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Network()
        {
            Hosts = new HashSet<Host>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        [Required, RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "Please enter a valid IP address.")]
        [Display(Name = "IP Address")]
        [StringLength(16)]
        public string IpAddress { get; set; }

        [Required, RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "Please enter a valid Net Mask.")]
        [Display(Name = "Subnet Mask")]
        [StringLength(64)]
        public string NetMask { get; set; }

        [Required, RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "Please enter a valid IP address.")]
        [Display(Name = "Default Gateway")]
        [StringLength(16)]
        public string DefaultGateway { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Host> Hosts { get; set; }
    }
}
