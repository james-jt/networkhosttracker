﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class HostType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        public virtual ICollection<Host> Hosts { get; set; }
    }
    //public enum HostType
    //{
    //    Server,
    //    Desktop,
    //    Laptop,
    //    Router,
    //    Firewall,
    //    Tablet,
    //    Phone,
    //    Television
    //}
}
