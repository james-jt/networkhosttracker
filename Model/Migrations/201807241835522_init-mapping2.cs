namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initmapping2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.HostAssignments1");
            AddPrimaryKey("dbo.HostAssignments1", new[] { "NetworkId", "HostId", "IpAddress" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.HostAssignments1");
            AddPrimaryKey("dbo.HostAssignments1", new[] { "NetworkId", "HostId" });
        }
    }
}
