namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedVMconcept : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hosts", "PhysicalHostId", c => c.Int(nullable: false));
            CreateIndex("dbo.Hosts", "PhysicalHostId");
            AddForeignKey("dbo.Hosts", "PhysicalHostId", "dbo.Hosts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Hosts", "PhysicalHostId", "dbo.Hosts");
            DropIndex("dbo.Hosts", new[] { "PhysicalHostId" });
            DropColumn("dbo.Hosts", "PhysicalHostId");
        }
    }
}
