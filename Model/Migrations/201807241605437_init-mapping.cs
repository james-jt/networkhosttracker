namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initmapping : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HostAssignments1",
                c => new
                    {
                        NetworkId = c.Int(nullable: false),
                        HostId = c.Int(nullable: false),
                        IpAddress = c.String(nullable: false, maxLength: 16),
                        DateAssigned = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.NetworkId, t.HostId })
                .ForeignKey("dbo.Hosts", t => t.HostId, cascadeDelete: true)
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.NetworkId)
                .Index(t => t.HostId);
            
            CreateTable(
                "dbo.Hosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 128),
                        DomainName = c.String(maxLength: 64),
                        DefaultNetwork = c.Int(nullable: false),
                        DefaultNetworkIpAddress = c.String(maxLength: 16),
                        Make = c.String(maxLength: 64),
                        LocationId = c.Int(nullable: false),
                        HostTypeId = c.Int(nullable: false),
                        IsPhysical = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HostTypes", t => t.HostTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => new { t.Name, t.DomainName }, unique: true, name: "DomainNameAndName")
                .Index(t => t.LocationId)
                .Index(t => t.HostTypeId);
            
            CreateTable(
                "dbo.HostTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Networks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 128),
                        IpAddress = c.String(nullable: false, maxLength: 16),
                        NetMask = c.String(nullable: false, maxLength: 64),
                        DefaultGateway = c.String(nullable: false, maxLength: 16),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.HostAssignments",
                c => new
                    {
                        HostId = c.Int(nullable: false),
                        NetworkId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HostId, t.NetworkId })
                .ForeignKey("dbo.Hosts", t => t.HostId, cascadeDelete: true)
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.HostId)
                .Index(t => t.NetworkId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HostAssignments1", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.HostAssignments1", "HostId", "dbo.Hosts");
            DropForeignKey("dbo.HostAssignments", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.HostAssignments", "HostId", "dbo.Hosts");
            DropForeignKey("dbo.Hosts", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Hosts", "HostTypeId", "dbo.HostTypes");
            DropIndex("dbo.HostAssignments", new[] { "NetworkId" });
            DropIndex("dbo.HostAssignments", new[] { "HostId" });
            DropIndex("dbo.Networks", new[] { "Name" });
            DropIndex("dbo.Locations", new[] { "Name" });
            DropIndex("dbo.HostTypes", new[] { "Name" });
            DropIndex("dbo.Hosts", new[] { "HostTypeId" });
            DropIndex("dbo.Hosts", new[] { "LocationId" });
            DropIndex("dbo.Hosts", "DomainNameAndName");
            DropIndex("dbo.HostAssignments1", new[] { "HostId" });
            DropIndex("dbo.HostAssignments1", new[] { "NetworkId" });
            DropTable("dbo.HostAssignments");
            DropTable("dbo.Networks");
            DropTable("dbo.Locations");
            DropTable("dbo.HostTypes");
            DropTable("dbo.Hosts");
            DropTable("dbo.HostAssignments1");
        }
    }
}
