namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class applications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 128),
                        PrimaryHost = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.ApplicationAssignments",
                c => new
                    {
                        ApplicationId = c.Int(nullable: false),
                        HostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationId, t.HostId })
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: true)
                .ForeignKey("dbo.Hosts", t => t.HostId, cascadeDelete: true)
                .Index(t => t.ApplicationId)
                .Index(t => t.HostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationAssignments", "HostId", "dbo.Hosts");
            DropForeignKey("dbo.ApplicationAssignments", "ApplicationId", "dbo.Applications");
            DropIndex("dbo.ApplicationAssignments", new[] { "HostId" });
            DropIndex("dbo.ApplicationAssignments", new[] { "ApplicationId" });
            DropIndex("dbo.Applications", new[] { "Name" });
            DropTable("dbo.ApplicationAssignments");
            DropTable("dbo.Applications");
        }
    }
}
