namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class applications1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationAssignments1",
                c => new
                    {
                        ApplicationId = c.Int(nullable: false),
                        HostId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationId, t.HostId })
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: true)
                .ForeignKey("dbo.Hosts", t => t.HostId, cascadeDelete: true)
                .Index(t => t.ApplicationId)
                .Index(t => t.HostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationAssignments1", "HostId", "dbo.Hosts");
            DropForeignKey("dbo.ApplicationAssignments1", "ApplicationId", "dbo.Applications");
            DropIndex("dbo.ApplicationAssignments1", new[] { "HostId" });
            DropIndex("dbo.ApplicationAssignments1", new[] { "ApplicationId" });
            DropTable("dbo.ApplicationAssignments1");
        }
    }
}
