﻿namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ApplicationAssignment
    {
        [Key]
        [Required]
        [Column(Order = 0)]
        [Display(Name = "Application")]
        public int ApplicationId { get; set; }
         
        [Key]
        [Required]
        [Column(Order = 1)]
        [Display(Name = "Host")]
        public int HostId { get; set; }

        [Display(Name = "Date Assigned")]
        public DateTime DateAssigned { get; set; }

        public virtual Host Host { get; set; }
        public virtual Application Application { get; set; }
    }
}
