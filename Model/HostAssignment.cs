﻿namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HostAssignment
    {
        [Key]
        [Required]
        [Column(Order = 0)]
        [Display(Name = "Network")]
        public int NetworkId { get; set; }

        [Key]
        [Required]
        [Column(Order = 1)]
        [Display(Name = "Host")]
        public int HostId { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required, RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "Please enter a valid IP address.")]
        [Display(Name = "IP Address")]
        [StringLength(16)]
        public string IpAddress { get; set; }

        [Display(Name = "Date Assigned")]
        public DateTime DateAssigned { get; set; }

        public virtual Host Host { get; set; }
        public virtual Network Network { get; set; }
    }
}
